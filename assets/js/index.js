$(document).ready(function(){
    // console.log(date());
    let source = [
        {
          "url" : "assets/img/background/1.jpeg",
          "author_name" : "Émile Perron",
          "author_url" : "https://unsplash.com/@emilep"
        },
        {
          "url" : "assets/img/background/2.jpeg",
          "author_name" : "Émile Perron",
          "author_url" : "https://unsplash.com/@emilep"
        },
        {
          "url" : "assets/img/background/3.jpeg",
          "author_name" : "Fatos Bytyqi",
          "author_url" : "https://unsplash.com/@fatosi",
        },
        {
          "url" : "assets/img/background/4.jpeg",
          "author_name" : "Blake Connally",
          "author_url" : "https://unsplash.com/@blakeconnally",
        },
        {
          "url" : "assets/img/background/5.jpeg",
          "author_name" : "Blake Connally",
          "author_url" : "https://unsplash.com/@blakeconnally",
        },
        {
          "url" : "assets/img/background/6.jpeg",
          "author_name" : "Ilya Pavlov",
          "author_url" : "https://unsplash.com/@ilyapavlov",
        },
        {
          "url" : "assets/img/background/7.jpeg",
          "author_name" : "Adi Goldstein",
          "author_url" : "https://unsplash.com/@adigold1",
        },
        {
          "url" : "assets/img/background/8.jpeg",
          "author_name" : "Oskar Yildiz",
          "author_url" : "https://unsplash.com/@oskaryil",
        },
        {
          "url" : "assets/img/background/9.jpeg",
          "author_name" : "Fabian Grohs",
          "author_url" : "https://unsplash.com/@grohsfabian",
        },
        {
          "url" : "assets/img/background/10.jpeg",
          "author_name" : "Fabian Grohs",
          "author_url" : "https://unsplash.com/@grohsfabian",
        },
        {
          "url" : "assets/img/background/11.jpeg",
          "author_name" : "Caspar Camille Rubin",
          "author_url" : "https://unsplash.com/@casparrubin",
        },
        {
          "url" : "assets/img/background/12.jpeg",
          "author_name" : "Chris Ried",
          "author_url" : "https://unsplash.com/@cdr6934",
        },
        {
          "url" : "assets/img/background/13.jpeg",
          "author_name" : "Fabian Grohs",
          "author_url" : "https://unsplash.com/@grohsfabian",
        },
        {
          "url" : "assets/img/background/14.jpeg",
          "author_name" : "Caspar Camille Rubin",
          "author_url" : "https://unsplash.com/@casparrubin",
        },
        {
          "url" : "assets/img/background/15.jpeg",
          "author_name" : "Temple Cerulean",
          "author_url" : "https://unsplash.com/@templecerulean",
        },
    ];
    // console.log(source);

    let quotes = [
      "Talk is cheap. Show me the code.",
      "When you don't create things, you become defined by your tastes rather than ability. your tastes only narrow & exclude people. so create.",
      "Programs must be written for people to read, and only incidentally for machines to execute.",
      "I'm not a great programmer; I'm just a good programmer with great habits.",
      "Give a man a program, frustrate him for a day. Teach a man to program, frustrate him for a lifetime.",
      "Truth can only be found in one place: the code.",
      "The most disastrous thing that you can ever learn is your first programming language.",
      "The most important property of a program is whether it accomplishes the intention of its user",
      "Happiness should be a function without any parameters.",
      "if you can write 'hello world' you can change the world",
      "Everyday life is like programming, I guess. If you love something you can put beauty into it."
    ];

    function changeData(){
        let value = Math.ceil(Math.random() * source.length)
        let src = source[value-1];
        $("#content_id").css("background", "url("+src['url']+")");
        $("#tribute_id").html(""+src['author_name']);
        $("#tribute_id").attr("href", src['author_url']);
    }

    function changeQuote(){
      let value = Math.ceil(Math.random() * quotes.length)
      let src = quotes[value-1];
      $("#quote_id").html(src);
    }

    function updateTime(){
        let d = new Date();
        let hour = d.getHours()
        hour = (hour.toString().length == 1) ? "0"+hour : hour;
        
        let minute = d.getMinutes();
        minute = (minute.toString().length == 1) ? "0"+minute : minute;
        $("#hour_id").html(hour+":"+minute);
        
        let date = d.toString().substr(0, 15);
        $("#date_id").html(date);
    }

    changeData();
    changeQuote();
    setInterval( function(){ changeData(); changeQuote(); } , 30000);

    updateTime();
    let d = new Date();
    let sec = d.getSeconds();
    setTimeout(() => {
        let d = new Date();
        updateTime();
        setInterval( function(){ updateTime() } , 60000);
    }, 61000-(sec*1000));
})